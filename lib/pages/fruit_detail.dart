import 'package:flutter/material.dart';
import 'package:fruit_listview/models/fruit_data_model.dart';

class FruitDetail extends StatelessWidget {
  final FruitDataModel fruitDataModel;
  // const FruitDetail({Key? key, required this.fruitDataModel}) : super(key: key);
  const FruitDetail({super.key, required this.fruitDataModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(fruitDataModel.name),),
      body: Container(
        padding: const EdgeInsets.only(left: 15.5, top: 5.5, right: 15.5, bottom: 5.5),
        child: Column(
          children: [
            Image.network(
              fruitDataModel.imageUrl,
              height: 500,
              fit: BoxFit.fill,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              fruitDataModel.desc,
              style: TextStyle(fontWeight: FontWeight.w100,fontSize: 20),
            )
          ],
        ),
      ),
    );
  }
}
