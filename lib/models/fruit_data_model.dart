class FruitDataModel {
  final String name, imageUrl, desc;
  // constructor
  FruitDataModel(this.name, this.imageUrl, this.desc);
}
